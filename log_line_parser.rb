class LogLineParser
    def initialize(line)
      level, line = line.split(":")
      @line = line.strip
      @level = level.gsub(/\[|\]/, "").downcase
    end
    def message
      @line
    end
    def log_level
      @level
    end
    def reformat
      "#{@line} (#{@level})"
    end
  end


  puts LogLineParser.new('[ERROR]: Invalid operation').message

  puts LogLineParser.new("[WARNING]:  Disk almost full\r\n").message

  puts LogLineParser.new('[ERROR]: Invalid operation').log_level

  puts LogLineParser.new('[INFO]: Operation completed').reformat