class Diamond
    ALPHABET = [*?A..?Z]
    def self.make_diamond(letter)
      index  = ALPHABET.index(letter)
      num    = (index + 1) * 2
      total  = (num).odd? ? num : num - 1
      final  = ''
      spaces = ' ' * total
      0.upto(index).each do |idx|
        spaces[index - idx] = ALPHABET[idx]
        spaces[(total - 1) - (index - idx)] = ALPHABET[idx] unless idx.zero?      
        final << "#{spaces}\n"
        spaces = ' ' * total
      end
      "#{final}#{final.split(/(?<=[\n])/)[0..-2].reverse.join}"
    end
  end


Diam = Diamond.make_diamond("Z")
puts Diam